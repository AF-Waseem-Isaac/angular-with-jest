# Angular-cli app using Jest test runner.

  

This project is just a reference app using [Jest Preset Angular](https://github.com/thymikee/jest-preset-angular) package to setup the Jest basic configuration for Angular projects.

The project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.15.

  
## Running unit tests
-  `npm run test` to just execute the unit tests via [Jest](https://github.com/facebook/jest).
-  `npm run test:watch` to excute the unit tests in the watch mode.
-  `npm test:coverage` to excute the unit tests and additionally generate the testing coverage report.

## Guiding steps

>  **Setup Jest test runner configurations**
- Generate the app using ng new {APP_NAME}
- run `npm install -D jest jest-preset-angular @types/jest`
- Copy `setup-jest.ts` , `jest.config.js` and `jest-global-mocks.ts` files into your project root path.
- Remove any code comment inside `tsconfig.json` and `tsconfig.spec.json`, As Jest will access and parse those files for further logic.

> **Jest syntax**

  Jest build on top of jasmine. And [@types/jest](https://www.npmjs.com/package/@types/jest) already defines most of jasmine types, So they have similar syntax for writing the specs, 
  You won't change almost anything in your written specs except for few things,
  For example (IBNLT) : 
 -  **Jasmine** `spyOn(Object,function).and.callThrough()` 
  **must be** `jest.spyOn(Object,function)` which -by defatul- tracks all calls to that function and delegate to its actual implementation.
  
 -   **jasmine** `it(() => {..})`
	**could be** `test(() => {...})` [*Optional*]
 

>  **Remove Karma test runner configurations**
- Remove all Karma dependencies by: `npm uninstall karma karma-chrome-launcher karma-coverage-istanbul-reporter karma-jasmine karma-jasmine-html-reporter` .
- Remove `karma.config.js` & `test.ts`files along with the `test` section in `angular.json` file. (found under `projects.{APP_NAME}.architect.test`)
- [*Optional*] : in `tsconfig.spec.json` file, you could add `jest` to `compilerOptions.types` and re-write your cases to use the jest syntax or keep using `jasmine` types. (*You could add & use both btw*).
