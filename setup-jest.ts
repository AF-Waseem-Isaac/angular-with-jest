// Assuming that your global setup file is setup-jest.ts
import 'jest-preset-angular/setup-jest';

/**
 * jest-preset-angular uses JSDOM which is different from normal browsers. 
 * You might need some global browser mocks to stimulate the behaviors of real browsers in JSDOM.
 * see : https://thymikee.github.io/jest-preset-angular/docs/getting-started/installation#global-mocks
 */
import './jest-global-mocks';