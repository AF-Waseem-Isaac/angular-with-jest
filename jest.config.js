/** Un-comment the following imports if u use custom paths for short import statement (inside tsconfig.compilerOptions.paths)  */
// const { pathsToModuleNameMapper } = require('ts-jest');
// const { paths } = require('./tsconfig.json')?.compilerOptions;

module.exports = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    globalSetup: 'jest-preset-angular/global-setup',
    testMatch: ['**/+(*.)+(spec).+(ts)'],
    // Reference : https://jestjs.io/docs/configuration#transform-objectstring-pathtotransformer--pathtotransformer-object
    transform: { '^.+.(ts|mjs|js|html)$': 'jest-preset-angular' },
    // Reference : https://jestjs.io/docs/configuration#transformignorepatterns-arraystring
    transformIgnorePatterns: [
      // Add any additionla es node_modules which aren't supported by the Jest compiler
      'node_modules/(?!@angular|rxjs|ngx-toastr|lodash-es|lodash|ngx-ui-tour-md-menu|ngx-ui-tour-core|ngx-mat-intl-tel-input)',
      'node_modules/(?!.*.js$)'
    ],

    /** Un-comment the following line if u use custom paths for short import statement (inside tsconfig.compilerOptions.paths)  */
    // moduleNameMapper: pathsToModuleNameMapper(paths, { prefix: '<rootDir>' }),
  };